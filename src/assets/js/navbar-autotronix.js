window.onload = function () {
  var options = {rotation: 'none', easing: 'linear', duration: 640};
  var svgMorpheus = new SVGMorpheus('#navbar-menu', options);
  var opened = false;

  $('#navbar-menu path').not(':nth-child(1)').click(function () {
    var windowWidth = $(window).width();
    console.log(windowWidth);
    if(opened) {
      if(windowWidth > 974) {
        svgMorpheus.to('botton-menu-closed', options);
      }
      $('.menu-desktop').removeClass('opened');
      opened = false;
      $('#navbar-menu').removeClass('opened');
    }
    else {
      if(windowWidth > 974) {
        svgMorpheus.to('botton-menu-opened', options, function () {
          $('.menu-desktop').addClass('opened');
        });
      }
      else {
        $('.menu-desktop').addClass('opened');
      }
      opened=true;
      $('#navbar-menu').addClass('opened');
    }
  });
};
