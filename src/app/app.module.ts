import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import {NgModule, LOCALE_ID} from '@angular/core';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import { NgxCaptchaModule } from 'ngx-captcha';
import {RouterModule} from '@angular/router';
import { registerLocaleData } from '@angular/common';

import {AppComponent} from './app.component';
import {CoursesRecentsComponent} from './components/home/courses-recents/courses-recents.component';
import {CoverComponent} from './components/home/cover/cover.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {FooterComponent} from './components/footer/footer.component';

import {ApiServiceService} from './services/api-service.service';
import {ContactComponent} from './components/contact/contact.component';
import {FooterBlocksComponent} from './components/footer-blocks/footer-blocks.component';
import {LogotypeComponent} from './components/logotype/logotype.component';
import {AutotronixModalComponent} from './components/autotronix-modal/autotronix-modal.component';
import {EnrollComponent} from './components/enroll/enroll.component';
import {AutotronixModalService} from './components/autotronix-modal/autotronix-modal.service';

import {CourseComponent} from './components/course/course.component';
import {HomeComponent} from './components/home/home/home.component';
import {AccessoryComponent} from './components/accessory/accessory.component';
import { CategoriesBlockComponent } from './components/categories-block/categories-block.component';
import { EventComponent } from './components/event/event.component';
import { OrderComponent } from './components/order/order.component';
import { YoutubeSafeUrlPipe } from './youtube-safe-url.pipe';

import localeEsAr from '@angular/common/locales/es-AR';

registerLocaleData(localeEsAr, 'es-Ar');

@NgModule({
  declarations: [
    AppComponent,
    CoursesRecentsComponent,
    CoverComponent,
    FooterComponent,
    NavbarComponent,
    ContactComponent,
    FooterBlocksComponent,
    LogotypeComponent,
    CourseComponent,
    HomeComponent,
    AutotronixModalComponent,
    EnrollComponent,
    AccessoryComponent,
    CategoriesBlockComponent,
    EventComponent,
    OrderComponent,
    YoutubeSafeUrlPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ReactiveFormsModule,
    NgxCaptchaModule,
    SlickCarouselModule,
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent,
        data: {title: 'Inicio'}
      },
      {
        path: 'cursos/:calias',
        component: CourseComponent,
        data: {title: 'Curso'}
      },
      {
        path: 'equipamiento',
        component: AccessoryComponent,
        data: {title: 'Equipamiento'}
      },
      {
        path: 'equipamiento/:aalias',
        component: AccessoryComponent,
        data: {title: 'Equipamiento'}
      },
      {
        path: 'eventos',
        component: EventComponent,
        data: {title: 'Eventos'}
      },
      {
        path: 'eventos/:ealias',
        component: EventComponent,
        data: {title: 'Eventos'}
      }
    ]),
  ],
  providers: [
    ApiServiceService,
    { provide: LOCALE_ID, useValue: 'es-Ar' },
    AutotronixModalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
