import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Cover, Contact } from '../model/cover';

import { Constants } from '../constants';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class ApiServiceService {

  constructor(private http: HttpClient) {
  }

  public getCoursesRecents() {
    console.log(Constants.API_URL + 'api/offerings/bol/recents');
    return this.http.get(Constants.API_URL + 'api/offerings/bol/recents');
  }

  public getCover(): Observable<Cover[]> {
    console.log(Constants.API_URL + 'api/cover/bol/list');
    return this.http.get<Cover[]>(Constants.API_URL + 'api/cover/bol/list');
  }

  public sendForm(data): Observable<any> {
    console.log(Constants.API_URL + 'api/form', data);
    return this.http.post(Constants.API_URL + 'api/form', data, httpOptions);
  }

  public getContent(data): Observable<any> {
    console.log(Constants.API_URL + 'api/content', data);
    return this.http.put(Constants.API_URL + 'api/content', data, httpOptions);
  }

  public enrollForm(data): Observable<any> {
    return this.http.post(Constants.API_URL + 'api/enrolls', data, httpOptions);
  }

  public allEvents(data): Observable<any> {
    return this.http.post(Constants.API_URL + 'api/events/all', data, httpOptions);
  }

  public all(name): Observable<any> {
    const url = Constants.API_URL + 'api/' + name;
    console.log(url);
    return this.http.get(url, httpOptions);
  }

  public create(name, data): Observable<any> {
    const url = Constants.API_URL + 'api/' + name;
    console.log(url, data);
    return this.http.post(url, data, httpOptions);
  }

  public read(name, id): Observable<any> {
    const url = Constants.API_URL + 'api/' + name + '/' + id;
    console.log(url);
    return this.http.get(url, httpOptions);
  }

  public update(name, id, data): Observable<any> {
    const url = Constants.API_URL + 'api/' + name + '/' + id;
    console.log(url, data);
    return this.http.put(url, data, httpOptions);
  }

  public delete(name, id): Observable<any> {
    const url = Constants.API_URL + 'api/' + name + '/' + id;
    console.log(url);
    return this.http.delete(url, httpOptions);
  }
}
