export class Constants {
  public static get API_URL(): string {
    //return 'http://localhost:8000/';
    return 'https://zeus.autotronix.com.bo/';
  }
  public static get CAPTCHA_KEY(): string {
    return '6LfjP58UAAAAAM_wcz5gvZrANXLQhoNmG_3Mgn79';
  }

  public static get IMAGE_URL(): string {
    //return 'http://localhost:8000/uploads/images';
    return 'https://zeus.autotronix.com.bo/uploads/images';
  }
}
