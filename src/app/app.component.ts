import { AfterViewChecked, Component, ElementRef, HostListener, Renderer2, ViewEncapsulation } from '@angular/core';
import {NavigationStart, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements AfterViewChecked {
  title = 'app';

  public navigationSubscription;
  public current_url;

  constructor( private el: ElementRef,
               private renderer: Renderer2,
               private _router: Router ) {
    this.navigationSubscription = this._router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationStart) {
        this.current_url = e.url;
        console.log(this.current_url);
      }
    });
  }

  ngAfterViewChecked() {
    this.onResize();
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    const elemPage = this.el.nativeElement.getElementsByClassName('page-content');
    const elemFooter = this.el.nativeElement.getElementsByClassName('footer-content');
    if (elemPage[0] !== undefined && elemPage[0] !== undefined) {
      const footerHeight = elemFooter[0].clientHeight;
      elemPage[0].style.minHeight = "calc(100vh - " + footerHeight + "px)";
    }
  }
}
