export class Category {
  id: number;
  name: String;
  image: String;
  alias: String;
  order: Number;
}

export class Accessory {
  id: number;
  model: String;
  brand: String;
  description: String;
  image: String;
  price: number;
  coin: String;
  quantity: number;
  country_basecode: String;
  categoryid: number;
}
