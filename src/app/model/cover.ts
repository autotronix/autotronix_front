export class Cover {
  offering: Offering;
  id: number;
}

export class Offering {
  city: string;
  coin: string;
  country_base: Country;
  country_code: string;
  course: Course;
  courseid: number;
  created_at: string;
  date: string;
  date_range: string;
  description: string;
  design: Design;
  designid: number;
  id: number;
  price: number;
  schedules: Schedule[];
  enrolments: Student[];
  updated_at: string;
}

export class Country {
  code: string;
  name: string;
  flag: string;
  official_coin: string;
  created_at: string;
  updated_at: string;
}

export class Course {
  id: number;
  designid: number;
  title: string;
  description: string;
  slogan: string;
  directed_to: string;
  objectives: string;
  created_at: string;
  updated_at: string;
  design: Design;
  offerings: Offering[];
}

export class Design {
  id: number;
  banner_big: string;
  banner_small: string;
  youtube_video: string;
  created_at: string;
  updated_at: string;
}

export class Schedule {
  date_ini: string;
  duration: number;
  offeringid: number;
  created_at: string;
  updated_at: string;
}

export class Student {
  higer_degree_studies: string;
  year_degree: number;
  center_degree: number;
  personaid: number;
  created_at: string;
  updated_at: string;
}

export class Contact {
  firstname: string;
  lastname: string;
  email: string;
  subject: string;
  message: string;
}

class Person {
  firstname: string;
  lastname: string;
  address: string;
  email: string;
  birthday: string;
  sex: string;
  country: string;
  city: string;
  phonenumber: string;
  facebook: string;
  photography: string;
}

export class Event {
  country_basecode: string;
  responsableid: number;
  name: string;
  information: string;
  date_ini: Date;
  duration: number;
  banner: string;
  city: string;
  address: string;
  capacity: number;
  person: Person;
}

export class Content {
  id: number;
  name: string;
  content: string;
  status: number;
  menu_itemid: number;
}
