import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../../services/api-service.service';
import { ActivatedRoute } from '@angular/router';
import { Constants } from '../../constants';
import { Offering } from '../../model/cover';
import {AutotronixModalService} from '../autotronix-modal/autotronix-modal.service';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent implements OnInit {

  public data: Offering;
  private id: number;
  private sub: any;
  public endDate: string;
  public isPast: boolean;
  public timeDiff: number;
  public countDouwn: string[];
  public academicHours: number;
  public inscribed: number;

  public IMAGE_URL = Constants.IMAGE_URL;

  constructor(
    private modalService: AutotronixModalService,
    private apiService: ApiServiceService,
    private route: ActivatedRoute
  ) {
    this.timeDiff = 0;
    this.academicHours = 0;
    this.inscribed = 0;
    this.countDouwn = [];
  }

  ngOnInit() {
    this.sub = this.route.params
      .subscribe(params => {
        this.apiService.read('offerings', params['calias'])
          .subscribe(
            res => {
              if (res.response === true) {
                this.data = res.data;
                this.data.price = Number(this.data.price);
                const lastIndex = this.data.schedules.length - 1;
                if (lastIndex >= 0) {
                  this.endDate = this.data.schedules[lastIndex].date_ini;
                  this.startCountdown();
                }

                this.inscribed = this.data.enrolments.length;

                this.academicHours = 0;
                for (const schedule of this.data.schedules) {
                  this.academicHours = this.academicHours + schedule.duration;
                }
                this.academicHours = Math.floor(this.academicHours / 60);
                //console.log(this.data);
              }
            }
          );
      });
  }

  controlIsPast() {
    if (this.endDate) {
      const nowDate = new Date();
      const now = nowDate.getTime();
      const endDate = new Date(this.endDate);
      this.timeDiff = endDate.getTime() - now;
      //console.log(endDate.getHours()+":"+endDate.getMinutes()+":"+endDate.getSeconds(),
      //  nowDate.getHours()+":"+nowDate.getMinutes()+":"+nowDate.getSeconds());
      this.isPast = this.timeDiff > 0;
    } else {
      this.isPast = false;
    }
  }

  startCountdown() {
    const interval = setInterval(() => {
      this.controlIsPast();
      const ss = Math.floor(this.timeDiff / 1000);
      const se = ss % 60;
      this.countDouwn[0] = se < 10 ? '0' + se : se.toString();
      const mm = Math.floor( ss / 60 );
      const mi = mm % 60;
      this.countDouwn[1] = mi < 10 ? '0' + mi : mi.toString();
      const hh = Math.floor( mm / 60 );
      const ho = hh % 24;
      this.countDouwn[2] = ho < 10 ? '0' + ho : ho.toString();
      const da = Math.floor( hh / 24 );
      this.countDouwn[3] = da < 10 ? '0' + da : da.toString();
      console.log(this.countDouwn);
      if (this.timeDiff < 0 ) {
        clearInterval(interval);
        console.log('Ding!');
      }
    }, 1000);
  }

  openModal() {
    this.modalService.toggle();
  }
}
