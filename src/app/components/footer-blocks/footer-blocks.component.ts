import { Component, OnInit } from '@angular/core';
import {ApiServiceService} from '../../services/api-service.service';
import {Content} from '../../model/cover';

@Component({
  selector: 'app-footer-blocks',
  templateUrl: './footer-blocks.component.html',
  styleUrls: ['./footer-blocks.component.scss']
})
export class FooterBlocksComponent implements OnInit {

  slogan: Content = null;
  dedication: Content = null;
  mission: Content = null;
  view: Content = null;
  footer: Content = null;

  constructor(
    private apiService: ApiServiceService
  ) { }

  ngOnInit() {
    this.getFooterContent();
  }

  private getFooterContent() {
    this.apiService.getContent({filter: 20})
      .subscribe((res: any) => {
        if (res.response === true) {
          const slogan = res.data.filter((a: Content) =>  a.id === 1);
          if (slogan !== null) {
            this.slogan = slogan[0];
          }
          const dedication = res.data.filter((a: Content) =>  a.id === 2);
          if (dedication !== null) {
            this.dedication = dedication[0];
          }
          const mission = res.data.filter((a: Content) =>  a.id === 3);
          if (mission !== null) {
            this.mission = mission[0];
          }
          const view = res.data.filter((a: Content) =>  a.id === 4);
          if (view !== null) {
            this.view = view[0];
          }
          const footer = res.data.filter((a: Content) =>  a.id === 5);
          if (footer !== null) {
            this.footer = footer[0];
          }
        }
      });
  }
}
