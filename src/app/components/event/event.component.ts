import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationStart, Router} from '@angular/router';
import {ApiServiceService} from '../../services/api-service.service';
import {Constants} from '../../constants';
import {Event} from '../../model/cover';

import * as moment from 'moment';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {
  public followingevents: Event[] = [];
  public pastevents: Event[] = [];
  public nextevent: Event = null;
  public eventItem: Event = null;
  public limit: number;
  public total: number;

  public IMAGE_URL = Constants.IMAGE_URL;

  constructor(private apiService: ApiServiceService,
              private route: ActivatedRoute,
              private router: Router) {
    this.limit = 10;
  }

  ngOnInit() {
    this.route.params
      .subscribe(params => {
        //console.log(params);
        if (params.ealias === undefined) {
          this.loadEvents();
        } else {
          this.apiService.read('events', params.ealias)
            .subscribe(
              res => {
                if (res.response === true) {
                  this.eventItem = res.data;
                }
              }
            );
        }
      });
  }

  moreEvents() {
    this.limit += 10;
    this.loadEvents();
  }

  loadEvents () {
    this.apiService.allEvents({limit: this.limit})
      .subscribe(
        res => {
          if (res.response === true) {
            this.followingevents = res.upcoming;
            this.pastevents = res.past;
            this.nextevent = res.next;
            this.total = res.total;
          }
        }
      );
  }

  toMonth(date): string {
    return moment(date).format('MMM');
  }

  toDate(event: Event): string {
    moment.locale('es');
    const date_str: string = moment(event.date_ini, 'YYYY-MM-DD')
      .add(event.duration, 'days').format('D [de] MMMM');
    return date_str + ', ' + event.city;
  }

  getRangeDate(event: Event): string {
    let res = '';

    moment.locale('es');

    const iniMonth = moment(event.date_ini).format('M');
    const endMonth = moment(event.date_ini).add(event.duration, 'days').format('M');

    let iniDate = moment(event.date_ini).format('D');
    if (iniMonth !== endMonth) {
      iniDate = moment(event.date_ini).format('D [de] MMMM');
    }
    const endDate = moment(event.date_ini).add(event.duration, 'days').format('D [de] MMMM');

    res = 'Del ' + iniDate + ' al ' + endDate;
    return res;
  }


  goEvent(event) {
    console.log(event);
    this.router.navigate(['/eventos', event.id]);
  }
}
