import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesRecentsComponent } from './courses-recents.component';

describe('CoursesRecentsComponent', () => {
  let component: CoursesRecentsComponent;
  let fixture: ComponentFixture<CoursesRecentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesRecentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesRecentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
