import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiServiceService } from '../../../services/api-service.service';

import { Constants } from '../../../constants';
import {Router} from '@angular/router';

@Component({
  selector: 'app-courses-recents',
  templateUrl: './courses-recents.component.html',
  styleUrls: ['./courses-recents.component.scss']
})
export class CoursesRecentsComponent implements OnInit {

  public recents: Object;
  public IMAGE_URL = Constants.IMAGE_URL;

  public slideConfig = {
    speed: 200,
    easing: 'ease-out',
    slidesToShow: 4,
    slidesToScroll: 1,
    startIndex: 1,
    draggable: false,
    infinite: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };

  constructor(private apiService: ApiServiceService,
              private router: Router) { }


  ngOnInit() {
    this.apiService.getCoursesRecents().subscribe(
     res => {
       this.recents = res;
     }
    );
  }


  goCourse(offering) {
    this.router.navigate(['/cursos', offering.id]);
  }

}
