import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiServiceService } from '../../../services/api-service.service';
import { Cover } from '../../../model/cover';

import { Constants } from '../../../constants';
import {Router} from '@angular/router';

@Component({
  selector: 'app-cover',
  templateUrl: './cover.component.html',
  styleUrls: ['./cover.component.scss']
})
export class CoverComponent implements OnInit {

  public cover: Cover[];
  public IMAGE_URL = Constants.IMAGE_URL;
  private indexSlide = 0;
  public currItem = null;

  public slideConfig = {
    speed: 200,
    easing: 'ease-out',
    slidesToShow: 3,
    slidesToScroll: 1,
    startIndex: 1,
    draggable: false,
    infinite: true,
    responsive: [
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };

  constructor(private apiService: ApiServiceService,
              private router: Router) { }


  ngOnInit() {
    this.apiService.getCover().subscribe(
      data => {
        this.cover = data;
        this.indexSlide = 0;
        this.currItem = this.cover[this.indexSlide];
        console.log(this.cover);
      }
    );
  }

  goCourse(offering) {
    console.log(offering);
    this.router.navigate(['/cursos', offering.id]);
  }

}
