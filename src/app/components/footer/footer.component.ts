// components/footer/footer.component.ts

import { Component, OnInit } from '@angular/core';

export class RRSS {
  id: number;
  name: string;
  class_f: string;
  url: string;
}

@Component({
  selector: 'app-footer',
  templateUrl: './footer.html',
  styleUrls: []
})
export class FooterComponent implements OnInit {

  public copyright: string;
  public rrss: RRSS[];

  constructor() { }

  ngOnInit() {
    const today = new Date();
    this.copyright = '© ' + today.getFullYear() + ' Autotronix Bolivia, Todos los derechos reservados';
    this. rrss = [
      { id: 1, name: 'facebook', class_f: '', url: '' },
      { id: 2, name: 'youtube', class_f: '', url: '' }
    ];
  }
}
