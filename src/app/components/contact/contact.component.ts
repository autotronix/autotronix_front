import { Component, ElementRef, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ReCaptchaV3Service } from 'ngx-captcha';

import { Contact } from '../../model/cover';

import { ApiServiceService } from '../../services/api-service.service';
import {Constants} from '../../constants';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  public contactForm: FormGroup;
  private siteKey: string = Constants.CAPTCHA_KEY;

  constructor(
    private apiService: ApiServiceService,
    private reCaptchaV3Service: ReCaptchaV3Service
  ) { }

  ngOnInit() {
    this.contactForm = new FormGroup({
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      email: new FormControl(),
      subject: new FormControl('', Validators.required),
      message: new FormControl('', Validators.required)
    });
  }

  resolved(captchaResponse: string) {
    console.log(this.contactForm.value);
  }

  onSubmit() {
    console.log(this.contactForm.valid);
    if (this.contactForm.valid) {
      this.reCaptchaV3Service
        .execute(this.siteKey, 'ContactComponent', (token) => {
          const data = this.contactForm.value;
          data.token = token;
          console.log(data);
          this.apiService.sendForm(data)
            .subscribe((res: Contact) => {
              console.log(res);
              this.contactForm.reset();
            }, error =>  console.error(error));
      });
    }
  }
}
