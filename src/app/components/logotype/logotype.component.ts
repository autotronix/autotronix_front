import {OnInit, Component, Input } from '@angular/core';

@Component({
  selector: 'app-logotype',
  templateUrl: './logotype.component.html',
  styleUrls: ['./logotype.component.scss']
})
export class LogotypeComponent implements OnInit {
  @Input('logotype') logotype: boolean;
  @Input('slogan') slogan: boolean;
  @Input('type') type: string;

  public thisClass: string;

  constructor() { }

  ngOnInit() {
    this.type = this.type === undefined ? 'none' : this.type;
    this.slogan = this.slogan !== false ? true : false;
    this.logotype = this.logotype !== false ? true : false;
  }

}
