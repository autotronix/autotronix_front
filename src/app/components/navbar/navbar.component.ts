// components/navbar/navbar.component.ts
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

export class Menu {
  label: string;
  state: string;
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.html',
  styleUrls: []
})
export class NavbarComponent implements OnInit {
  menu: Menu[] = [
    /*{
      'label': 'Cursos',
      'state': 'main'
    },*/
    {
      'label': 'Eventos',
      'state': '/eventos'
    },
    /*{
      'label': 'Recursos',
      'state': 'main'
    },*/
    {
      'label': 'Equipamiento',
      'state': '/equipamiento'
    }
  ];
  isCollapsed = true;


  constructor() { }

  ngOnInit() {
  }
}
