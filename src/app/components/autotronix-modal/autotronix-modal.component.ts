import {Component, Input, OnInit} from '@angular/core';
import {AutotronixModalService} from './autotronix-modal.service';

@Component({
  selector: 'app-autotronix-modal',
  templateUrl: './autotronix-modal.component.html',
  styleUrls: ['./autotronix-modal.component.scss']
})
export class AutotronixModalComponent implements OnInit {

  @Input() componentName: string;
  isOpen = false;

  constructor(
    private modalService: AutotronixModalService
  ) { }

  ngOnInit() {
    this.modalService.change.subscribe(isOpen => {
      this.isOpen = isOpen;
    });
  }

  closeModal() {
    this.modalService.toggle();
  }

}
