import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutotronixModalComponent } from './autotronix-modal.component';

describe('AutotronixModalComponent', () => {
  let component: AutotronixModalComponent;
  let fixture: ComponentFixture<AutotronixModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutotronixModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutotronixModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
