import {EventEmitter, Injectable, Output} from '@angular/core';

@Injectable()
export class AutotronixModalService {

  isOpen = false;
  public dataSelected;

  @Output() change: EventEmitter<boolean> = new EventEmitter();

  toggle() {
    this.isOpen = !this.isOpen;
    this.change.emit(this.isOpen);
  }

}
