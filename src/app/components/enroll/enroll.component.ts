import {Component, OnInit} from '@angular/core';
import {ApiServiceService} from '../../services/api-service.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-enroll',
  templateUrl: './enroll.component.html',
  styleUrls: ['./enroll.component.scss']
})
export class EnrollComponent implements OnInit {

  public enrollForm: FormGroup;
  public successMsg: String = '';
  public errorMsg: String = '';

  constructor(
    private apiService: ApiServiceService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {

    this.route.params
      .subscribe(params => {
        this.enrollForm = new FormGroup({
          offeringid: new FormControl(params['calias']),
          firstname: new FormControl('', Validators.required),
          lastname: new FormControl('', Validators.required),
          email: new FormControl(''),
          address: new FormControl(),
          sex: new FormControl('', Validators.required),
          country: new FormControl(),
          city: new FormControl()
        });
      });
  }

  onSubmit() {
    this.successMsg = '';
    this.errorMsg = '';
    if (this.enrollForm.valid) {
      this.apiService.enrollForm(this.enrollForm.value)
        .subscribe((res) => {
          this.successMsg = 'Sus datos han sido registrados correctamente, cierre el formulario para continuar';
          this.enrollForm.reset();
        }, error => {
          console.error(error);
          this.successMsg = 'No se pudo registrar su información, cierre el formulario para continuar o contáctese con el administrador';
        });
    }
  }
}
