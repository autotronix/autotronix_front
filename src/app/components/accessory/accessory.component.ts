import {Component, OnInit} from '@angular/core';
import {Constants} from '../../constants';
import {Accessory, Category} from '../../model/category';
import {ApiServiceService} from '../../services/api-service.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {AutotronixModalService} from '../autotronix-modal/autotronix-modal.service';
import {Content} from '../../model/cover';

@Component({
  selector: 'app-accessory',
  templateUrl: './accessory.component.html',
  styleUrls: ['./accessory.component.scss']
})
export class AccessoryComponent implements OnInit {
  public categories: Category[];
  public accessories: Accessory[];

  public categorySel: Category;

  public description: any;

  public API_URL: String;

  constructor(
    private apiService: ApiServiceService,
    private modalService: AutotronixModalService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.API_URL = Constants.API_URL;

    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
    }

    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        this.router.navigated = false;
        //window.scrollTo(0, 0);
      }
    });
  }

  ngOnInit() {
    this.apiService.all('category')
      .subscribe(
        res => {
          if (res.response === true) {
            this.categories = res.data;
            const alias = this.route.snapshot.params['aalias'];

            if (alias === undefined || alias === null) {
              this.categorySel = this.categories[0];
            } else {
              this.categorySel = this.categories.filter(x => x.alias === alias)[0];
            }
          }
        }
      );
    this.apiService.all('accessory')
      .subscribe(
        res => {
          if (res.response === true && this.categorySel!=undefined) {
            this.accessories = res.data.filter(x => x.categoryid === this.categorySel.id);
          }
        }
      );
    this.getContent();
  }

  private getContent() {
    this.apiService.getContent({filter: 22})
      .subscribe((res: any) => {
        if (res.response === true) {
          const slogan = res.data.filter((a: Content) => a.id === 6);
          if (slogan !== null) {
            this.description = slogan[0];
          }
        }
      });
  }

  openModal(accessory) {
    this.modalService.dataSelected = accessory;
    this.modalService.toggle();
  }
}
