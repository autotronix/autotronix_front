import {Component, Input, OnInit} from '@angular/core';
import {ApiServiceService} from '../../services/api-service.service';
import {Category} from '../../model/category';
import { Constants } from "../../constants";
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-categories-block',
  templateUrl: './categories-block.component.html',
  styleUrls: ['./categories-block.component.scss']
})
export class CategoriesBlockComponent implements OnInit {
  public categories: Category[];
  public categorySel: Category;

  @Input() public showHeader: string;

  public API_URL: String;

  constructor(
    private apiService: ApiServiceService,
    private route: ActivatedRoute,
    private router: Router
    ) {
    this.API_URL = Constants.API_URL;
  }

  ngOnInit() {
    this.apiService.all('category')
      .subscribe(
        res => {
          if (res.response === true) {
            this.categories = res.data;
            const alias = this.route.snapshot.params['aalias'];

            if(alias === undefined) {
              this.categorySel = new Category();
              this.categorySel.id = 0;
            } else {
              this.categorySel = this.categories.filter(x => x.alias === alias)[0];
            }

          }
        }
      );
  }


  goAccessoryCategory(alias: string) {
    this.router.navigate(['/equipamiento', alias]);
  }

}
