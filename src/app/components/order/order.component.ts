import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {ApiServiceService} from '../../services/api-service.service';
import {AutotronixModalService} from '../autotronix-modal/autotronix-modal.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  public orderForm: FormGroup;
  public successMsg: String = '';
  public errorMsg: String = '';

  constructor(
    private apiService: ApiServiceService,
    private modalService: AutotronixModalService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {

    this.route.params
      .subscribe(params => {
        this.orderForm = new FormGroup({
          offeringid: new FormControl(params['calias']),
          firstname: new FormControl('', Validators.required),
          lastname: new FormControl('', Validators.required),
          ci: new FormControl('', Validators.required),
          email: new FormControl('', Validators.required),
          phone: new FormControl('', [Validators.required, Validators.min(10000000000), Validators.max(999999999999)]),
          address: new FormControl('', Validators.required),
          city: new FormControl()
        });
      });
  }

  onSubmit() {
    this.successMsg = '';
    this.errorMsg = '';
    if (this.orderForm.valid) {
      const data = this.orderForm.value;
      data.accessoryid = this.modalService.dataSelected.id;

      this.apiService.create('order', data)
        .subscribe((res) => {
          this.successMsg = 'Sus datos han sido registrados correctamente, cierre el formulario para continuar';
          this.modalService.dataSelected = null;
          this.orderForm.reset();
        }, error => {
          console.error(error);
          this.successMsg = 'No se pudo registrar su información, cierre el formulario para continuar o contáctese con el administrador';
        });
    }
  }
}
